import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { People } from 'src/app/models/people';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  @Input() persona: People;
  @Output() detailEmmiter = new EventEmitter();
  constructor(private router: ActivatedRoute) { }

  ngOnInit() {
  }

  volver() {
    this.detailEmmiter.emit(false);
  }
}
