import { Films } from './../../../models/films';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() listaPeliculas: Films[] = [];
  @Output() peliculaEmmiter = new EventEmitter();
  @Output() paginadorEmmiter = new EventEmitter();
  @Input() total: number;
  @Input() siguiente: number;
  @Input() anterior: number;
  @Input() paginasFaltantes: number;
  paginaActual: number;
  constructor() { }

  ngOnInit() {
    this.paginaActual = 1;
  }

  irPelicula(pelicula: Films) {
    this.peliculaEmmiter.emit(pelicula);
  }
  paginador(pagina: number) {
    this.paginaActual = pagina;
    this.paginadorEmmiter.emit(pagina);
  }

}
