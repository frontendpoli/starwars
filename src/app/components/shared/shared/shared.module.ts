import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from '../nav/nav.component';
import { RouterModule } from '@angular/router';
import { AlertComponent } from '../alert/alert.component';
import { NocontentComponent } from '../nocontent/nocontent.component';

@NgModule({
  declarations: [ NavComponent, AlertComponent, NocontentComponent],
  imports: [
    CommonModule, RouterModule, HttpClientModule
  ],
  exports: [NavComponent, AlertComponent, NocontentComponent]
})
export class SharedModule { }
