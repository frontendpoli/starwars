import { Vehicles } from './../../models/vehicles';
import { Component, OnInit } from '@angular/core';
import { VehiclesService } from 'src/app/services/vehicles.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  detalle: boolean;
  listaVehiculos: Vehicles[] = [];
  vehiculo: Vehicles;
  total: number;
  siguiente: number;
  anterior: number;
  paginasFaltantes: number;
  constructor(public service: VehiclesService) { }

  ngOnInit() {
    this.loadVehiculoList();
  }
  inicializarPaginador() {
    this.siguiente = this.service.paginador.next;
    this.anterior = this.service.paginador.previous;
    this.total = this.service.paginador.counter;
    this.paginasFaltantes = this.service.paginador.missing;
  }

  loadVehiculoList(pagina?: string) {
    this.service.loadVehiculoList(pagina).subscribe(data => { this.listaVehiculos = data; this.inicializarPaginador(); });
  }

  loadVehiculo(nombre: string) {
     this.service.loadVehiculo(nombre).subscribe((data: Vehicles) => {
      this.vehiculo = data[0];
      this.detalle = this.vehiculo !== null && this.vehiculo !== undefined;
    });
  }

  recibeVehiculo(vehiculo: Vehicles) {
    this.detalle = true;
    this.vehiculo = vehiculo;
  }
  recibeVolver(detalle: boolean) {
    this.detalle = detalle;
  }
}
