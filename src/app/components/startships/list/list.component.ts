import { Startships } from './../../../models/startships';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input()  listaNaves: Startships[] = [];
  @Output() navesEmmiter = new EventEmitter();
  @Output() paginadorEmmiter = new EventEmitter();
  @Input()  total: number;
  @Input()  siguiente: number;
  @Input()  anterior: number;
  @Input()  paginasFaltantes: number;
  paginaActual: number;
  constructor() { }

  ngOnInit() {
    this.paginaActual = 1;
  }

  irNave(naves: Startships) {
    this.navesEmmiter.emit(naves);
  }
  paginador(pagina: number) {
    this.paginaActual = pagina;
    this.paginadorEmmiter.emit(pagina);
  }

}
