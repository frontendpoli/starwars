import { Startships } from './../../models/startships';
import { Component, OnInit } from '@angular/core';
import { StartshipsService } from 'src/app/services/startships.service';

@Component({
  selector: 'app-startships',
  templateUrl: './startships.component.html',
  styleUrls: ['./startships.component.css']
})
export class StartshipsComponent implements OnInit {

  detalle: boolean;
  listaNaves: Startships[] = [];
  nave: Startships;
  total: number;
  siguiente: number;
  anterior: number;
  paginasFaltantes: number;
  constructor(public service: StartshipsService) { }

  ngOnInit() {
    this.loadVaneList();
  }
  inicializarPaginador() {
    this.siguiente = this.service.paginador.next;
    this.anterior = this.service.paginador.previous;
    this.total = this.service.paginador.counter;
    this.paginasFaltantes = this.service.paginador.missing;
  }

  loadVaneList(pagina?: string) {
    this.service.loadNaves(pagina).subscribe(data => { this.listaNaves = data; this.inicializarPaginador(); });
  }

  loadNave(nombre: string) {
     this.service.loadNave(nombre).subscribe((data: Startships) => {
      this.nave = data[0];
      this.detalle = this.nave !== null && this.nave !== undefined;
    });
  }

  recibeNave(nave: Startships) {
    this.detalle = true;
    this.nave = nave;
  }
  recibeVolver(detalle: boolean) {
    this.detalle = detalle;
  }
}
