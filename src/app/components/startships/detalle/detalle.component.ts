import { Startships } from './../../../models/startships';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  @Input() nave: Startships;
  @Output() detailEmmiter = new EventEmitter();
  constructor(private router: ActivatedRoute) { }

  ngOnInit() {
  }

  volver() {
    this.detailEmmiter.emit(false);
  }
}
