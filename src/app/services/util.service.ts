import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

 /**
 * Metodo para saber paginas siguiente
 * @param {number} total
 * @returns valor de la pagina siguiente
 * @memberof UtilService
 */
  cargarIndice(valor: string): number {
    if (valor === undefined || valor === null) {
      return -1;
    }
    const cadena = valor.toString().charAt(valor.length - 1);
    const indice = Number(cadena);
    return indice;
  }
  /**
   * Metodo para calcular numero de paginas restantes
   * @param {number} total
   * @returns valor total de paginas restantes
   * @memberof UtilService
   */
  paginasrestantes(total: number): number {
    if ((total === undefined || total === null) && total <= 0) {
      return 0;
    }
    const totalPaginas = total / 10;
    return Math.round(totalPaginas);
  }
}
