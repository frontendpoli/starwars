import { Startships } from './../models/startships';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilService } from './util.service';
import { URI_SWAPI } from '../const/uriConsumo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class StartshipsService {

  private getStartShipsUrl: string;
  paginador: any = {};
  constructor(private http: HttpClient,
              private util: UtilService) {
    this.getStartShipsUrl = `${URI_SWAPI.url_swapi}starships/`;
  }

  loadNaves(pagina: string): Observable<Startships[]> {
    const URI = pagina === undefined || pagina === null ? this.getStartShipsUrl : `${this.getStartShipsUrl}?page=${pagina}`;
    return this.http.get(URI).pipe(
      map((e: any) => {
        this.paginador = { next: this.util.cargarIndice(e.next), previous: this.util.cargarIndice(e.previous),
                           counter: e.count, missing: this.util.paginasrestantes(e.count) };
        return e.results.map(ei => ei);
      })
    );
  }

  loadNave(nombre: string): Observable<Startships> {
    return this.http.get(`${URI_SWAPI.url_swapi}starships?search=${nombre}`)
      .pipe(
        map((e: any) => {
          return e.results.map(ei => ei);
        })
      );
  }
}
