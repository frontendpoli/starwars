import { Planets } from './../models/planets';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilService } from './util.service';
import { URI_SWAPI } from '../const/uriConsumo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PlantesService {

  private getPlanetsUrl: string;
  paginador: any = {};
  constructor(private http: HttpClient,
              private util: UtilService) {
    this.getPlanetsUrl = `${URI_SWAPI.url_swapi}planets/`;
  }

  loadPlanetas(pagina: string): Observable<Planets[]> {
    const URI = pagina === undefined || pagina === null ? this.getPlanetsUrl : `${this.getPlanetsUrl}?page=${pagina}`;
    return this.http.get(URI).pipe(
      map((e: any) => {
        this.paginador = { next: this.util.cargarIndice(e.next), previous: this.util.cargarIndice(e.previous),
                           counter: e.count, missing: this.util.paginasrestantes(e.count) };
        return e.results.map(ei => ei);
      })
    );
  }

  loadPlaneta(nombre: string): Observable<Planets> {
    return this.http.get(`${URI_SWAPI.url_swapi}planets?search=${nombre}`)
      .pipe(
        map((e: any) => {
          return e.results.map(ei => ei);
        })
      );
  }
}
