import { Vehicles } from './../models/vehicles';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilService } from './util.service';
import { URI_SWAPI } from '../const/uriConsumo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class VehiclesService {
  private getVehiclesUrl: string;
  paginador: any = {};
  constructor(private http: HttpClient,
              private util: UtilService) {
    this.getVehiclesUrl = `${URI_SWAPI.url_swapi}vehicles/`;
  }

  loadVehiculoList(pagina: string): Observable<Vehicles[]> {
    const URI = pagina === undefined || pagina === null ? this.getVehiclesUrl : `${this.getVehiclesUrl}?page=${pagina}`;
    return this.http.get(URI).pipe(
      map((e: any) => {
        this.paginador = { next: this.util.cargarIndice(e.next), previous: this.util.cargarIndice(e.previous),
                           counter: e.count, missing: this.util.paginasrestantes(e.count) };
        return e.results.map(ei => ei);
      })
    );
  }

  loadVehiculo(nombre: string): Observable<Vehicles> {
    return this.http.get(`${URI_SWAPI.url_swapi}vehicles?search=${nombre}`)
      .pipe(
        map((e: any) => {
          return e.results.map(ei => ei);
        })
      );
  }
}
